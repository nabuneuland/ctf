package net.seliba.ctf.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

/**
 * Class which handles when a Player
 * drops an item.
 */
public class PlayerDropListener implements Listener {

    /**
     * Executed when a Player drops an item.
     * Used to disable item dropping.
     * @param event The PlayerDropItemEvent, provided by Spigot
     */
    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent event) {

        //Always cancel the event
        event.setCancelled(true);

    }

}
