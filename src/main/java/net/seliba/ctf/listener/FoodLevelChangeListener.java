package net.seliba.ctf.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Class which handles any changes
 * of the food level bar.
 */
public class FoodLevelChangeListener implements Listener {

    /**
     * Executed when the food bar of a Player changes.
     * Used to disable hunger.
     * @param event The FoodLevelChangeEvent, provided by Spigot
     */
    @EventHandler
    public void onPlayerHungerChange(FoodLevelChangeEvent event) {

        //Always cancel the event
        event.setCancelled(true);

    }

}
