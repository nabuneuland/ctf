package net.seliba.ctf.listener;

import net.seliba.ctf.teams.TeamManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Class which handles Player damages.
 */
public class PlayerDamageByEntityListener implements Listener {

    private TeamManager teamManager;

    /**
     * The default constructor, used to inject dependencies
     * into the class.
     * @param teamManager The instance of the TeamManager
     */
    public PlayerDamageByEntityListener(TeamManager teamManager) {

        this.teamManager = teamManager;

    }

    /**
     * Executed when an Entity get damaged by another Entity.
     * Used to disable friendly fire.
     * @param event The EntityDamageByEntityEvent, provided by Spigot
     */
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent event) {

        //Check if the damaged Entity and the Entity who damaged are Players
        if(event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            //Define the Players
            Player damager = (Player) event.getDamager();
            Player target = (Player) event.getEntity();

            //Checks if the Players are in the same team
            if(teamManager.getTeam(damager) == teamManager.getTeam(target)) {
                //Cancel the event
                event.setCancelled(true);
            }
        }

    }

}
