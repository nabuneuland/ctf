package net.seliba.ctf.listener;

import net.seliba.ctf.Ctf;
import net.seliba.ctf.points.PointReason;
import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Class which handles Player deaths and respawns
 */
public class PlayerDeathListener implements Listener {

    private TeamManager teamManager;
    private PointsManager pointsManager;

    /**
     * The default constructor, used to inject dependencies
     * into the class.
     * @param teamManager The instance of the TeamManager
     * @param pointsManager The instance of the PointsManager
     */
    public PlayerDeathListener(TeamManager teamManager, PointsManager pointsManager) {

        this.teamManager = teamManager;
        this.pointsManager = pointsManager;

    }

    /**
     * Executed when a Player dies.
     * Used to change the death message, reset the flags
     * if needed, give points to the killer and respawn
     * the Player who died.
     * @param event The PlayerDeathEvent, provided by Spigot
     */
    @EventHandler
    public void onDeath(PlayerDeathEvent event) {

        //Define used variables
        Player player = event.getEntity();
        Player killer = player.getKiller();
        List<ItemStack> itemStacks = event.getDrops();

        //Change death message
        event.setDeathMessage("§7[§c†§7] " + player.getName());

        //Check if the death player had a flag
        if(hasBlueFlag(itemStacks)) {
            //Check if the player is under the map
            if(player.getLocation().getY() < 0) {
                //Reset the flag
                teamManager.resetFlag(Team.BLUE);
                Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.BLUE.getName() + " §awurde zurückgesetzt!");
            } else {
                //Drop the flag
                player.getLocation().getBlock().setType(Material.BLUE_BANNER);
                Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.BLUE.getName() + " §awurde fallengelassen!");
            }
        } else if(hasRedFlag(itemStacks)) {
            //Check if the player is under the map
            if(player.getLocation().getY() < 0) {
                //Reset the flag
                teamManager.resetFlag(Team.RED);
                Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde zurückgesetzt!");
            } else {
                //Drop the flag
                player.getLocation().getBlock().setType(Material.RED_BANNER);
                Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde fallengelassen!");
            }
        } else {
            if(killer != null) {
                //Add points
                pointsManager.addPoints(killer, PointReason.KILL);

                //Clear drops from the death Player
                event.getDrops().clear();

                //Respawn the Player instantly
                player.spigot().respawn();
                return;
            }
        }

        //Add flagman kill if the killer is not null
        if(killer != null) {
            pointsManager.addPoints(killer, PointReason.FLAGMAN_KILL);
        }

        //Clear drops from the death Player
        event.getDrops().clear();

        //Respawn the Player instantly
        player.spigot().respawn();

    }

    /**
     * Executed when a Player respawns.
     * Used to reset the Player after the respawn.
     * @param event The PlayerRespawnEvent, provided by Spigot
     */
    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {

        //Define the Player who respawned
        Player player = event.getPlayer();

        //Reset player with a short delay to fix various bugs
        Bukkit.getScheduler().runTaskLater(Ctf.getProvidingPlugin(Ctf.class), () ->
            teamManager.resetPlayer(player)
        , 3L);

    }

    /**
     * Checks if a List contains the flag of the blue team.
     * @param itemStacks The list which should be checked
     * @return Whether the list contained the blue flag or not
     */
    private boolean hasBlueFlag(List<ItemStack> itemStacks) {

        return itemStacks.stream()
                .map(ItemStack::getType)
                .anyMatch(material -> material == Material.BLUE_BANNER);

    }

    /**
     * Checks if a List contains the flag of the red team.
     * @param itemStacks The list which should be checked
     * @return Whether the list contained the red flag or not
     */
    private boolean hasRedFlag(List<ItemStack> itemStacks) {

        return itemStacks.stream()
                .map(ItemStack::getType)
                .anyMatch(material -> material == Material.RED_BANNER);

    }

}
