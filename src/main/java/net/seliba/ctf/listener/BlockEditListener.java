package net.seliba.ctf.listener;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * Class which handles block breaks and
 * block places.
 */
public class BlockEditListener implements Listener {

    /**
     * Executed when a Player breaks a Block.
     * Used to disable the block break.
     * @param event The BlockBreakEvent, provided by Spigot
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {

        //Return when the Player is in Creative mode
        if(event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }

        //Cancel the event
        event.setCancelled(true);

    }

    /**
     * Executed when a Player places a Block.
     * Used to disable the block place.
     * @param event The BlockPlaceEvent, provided by Spigot
     */
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {

        //Return when the Player is in Creative mode
        if(event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }

        //Cancel the event
        event.setCancelled(true);

    }

}
