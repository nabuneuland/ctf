package net.seliba.ctf.listener;

import net.seliba.ctf.points.PointReason;
import net.seliba.ctf.points.PointsManager;
import net.seliba.ctf.teams.Team;
import net.seliba.ctf.teams.TeamManager;
import net.seliba.ctf.utils.ItemStackBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Class which handles Player intactions.
 */
public class PlayerInteractListener implements Listener {

    private TeamManager teamManager;
    private PointsManager pointsManager;

    /**
     * The default constructor, used to inject dependencies
     * into the class.
     * @param teamManager The instance of the TeamManager
     * @param pointsManager The instance of the PointsManager
     */
    public PlayerInteractListener(TeamManager teamManager, PointsManager pointsManager) {

        this.teamManager = teamManager;
        this.pointsManager = pointsManager;

    }

    /**
     * Executed when the Player interacts to anything.
     * Used for flag capturing.
     * @param event The PlayerInteractEvent, provided by Spigot
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {

        //Define the Player and his team
        Player player = event.getPlayer();
        Team team = teamManager.getTeam(player);

        //Return if the clicked Block is null
        if(event.getClickedBlock() == null) {
            return;
        }

        //Send message and return if there aren't other Players on the server
        if(Bukkit.getOnlinePlayers().size() == 1) {
            player.sendMessage("§6CTF §7» §aBitte warte auf weitere Mitspieler!");
            return;
        }

        //Define the clicked Block
        Block block = event.getClickedBlock();

        //Check if the Block is the red flag
        if (block.getType() == Team.RED.getBannerMaterial()) {

            //Check if the flag is on the flag spot
            if(block.getLocation().getBlockX() == Team.RED.getFlagLocation().getBlockX() && block.getLocation().getBlockZ() == Team.RED.getFlagLocation().getBlockZ()) {
                //Check the team
                if(team == Team.BLUE) {
                    //Flag pickup
                    block.setType(Material.AIR);
                    player.getInventory().setHelmet(new ItemStackBuilder(Material.RED_BANNER).build());
                    Bukkit.broadcastMessage("§6CTF §7» §6" + player.getName() + " §ahat die Flagge von Team " + Team.RED.getName() + " §aeingesammelt!");
                    pointsManager.addPoints(player, PointReason.FLAG_PICKUP);
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge aufgehoben! §6+1 Punkt");
                    player.sendMessage("§6CTF §7» §aBringe sie zu deiner Basis zurück!");
                } else if(player.getInventory().getHelmet().getType() == Material.BLUE_BANNER) {
                    //Flag capturing
                    player.getInventory().setHelmet(null);
                    pointsManager.addPoints(player, PointReason.FLAG_CAPTURE);
                    Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.BLUE.getName() + " §awurde von §6" + player.getName() + " §aerobert!");
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge erobert! §6+10 Punkte");
                    teamManager.resetFlag(Team.BLUE);
                }
            } else {
                //Check the team
                if(team == Team.RED) {
                    //Flag recovering
                    block.setType(Material.AIR);
                    Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde zurückgesetzt!");
                    pointsManager.addPoints(player, PointReason.FLAG_RECOVER);
                    player.sendMessage("§6CTF §7» §aDu hast deine Flagge zurückerobert! §6+1 Punkt");
                    teamManager.resetFlag(Team.RED);
                } else if(team == Team.BLUE) {
                    //Flag pickup
                    block.setType(Material.AIR);
                    player.getInventory().setHelmet(new ItemStackBuilder(Material.RED_BANNER).build());
                    Bukkit.broadcastMessage("§6CTF §7» §6" + player.getName() + " §ahat die Flagge von Team " + Team.RED.getName() + " §aeingesammelt!");
                    pointsManager.addPoints(player, PointReason.FLAG_PICKUP);
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge aufgehoben! §6+1 Punkt");
                    player.sendMessage("§6CTF §7» §aBringe sie zu deiner Basis zurück!");
                }
            }

        //Check if the Block is the blue flag
        } else if(block.getType() == Team.BLUE.getBannerMaterial()) {

            //Check if the flag is on the flag spot
            if(block.getLocation().getBlockX() == Team.BLUE.getFlagLocation().getBlockX() && block.getLocation().getBlockZ() == Team.BLUE.getFlagLocation().getBlockZ()) {
                //Check the team
                if(team == Team.RED) {
                    //Flag pickup
                    block.setType(Material.AIR);
                    player.getInventory().setHelmet(new ItemStackBuilder(Material.BLUE_BANNER).build());
                    Bukkit.broadcastMessage("§6CTF §7» §6" + player.getName() + " §ahat die Flagge von Team " + Team.BLUE.getName() + " §aeingesammelt!");
                    pointsManager.addPoints(player, PointReason.FLAG_PICKUP);
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge aufgehoben! §6+1 Punkt");
                    player.sendMessage("§6CTF §7» §aBringe sie zu deiner Basis zurück!");
                } else if(player.getInventory().getHelmet().getType() == Material.RED_BANNER) {
                    //Flag capturing
                    teamManager.resetFlag(Team.RED);
                    player.getInventory().setHelmet(null);
                    pointsManager.addPoints(player, PointReason.FLAG_CAPTURE);
                    Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde von §6" + player.getName() + " §aerobert!");
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge erobert! §6+10 Punkte");
                }
            } else {
                //Check the team
                if(team == Team.BLUE) {
                    //Flag recovering
                    block.setType(Material.AIR);
                    Bukkit.broadcastMessage("§6CTF §7» §aDie Flagge von Team " + Team.RED.getName() + " §awurde zurückgesetzt!");
                    pointsManager.addPoints(player, PointReason.FLAG_RECOVER);
                    player.sendMessage("§6CTF §7» §aDu hast deine Flagge zurückerobert! §6+1 Punkt");
                    teamManager.resetFlag(Team.BLUE);
                } else if(team == Team.RED) {
                    //Flag pickup
                    block.setType(Material.AIR);
                    player.getInventory().setHelmet(new ItemStackBuilder(Material.RED_BANNER).build());
                    Bukkit.broadcastMessage("§6CTF §7» §6" + player.getName() + " §ahat die Flagge von Team " + Team.BLUE.getName() + " §aeingesammelt!");
                    pointsManager.addPoints(player, PointReason.FLAG_PICKUP);
                    player.sendMessage("§6CTF §7» §aDu hast die gegnerische Flagge aufgehoben! §6+1 Punkt");
                    player.sendMessage("§6CTF §7» §aBringe sie zu deiner Basis zurück!");
                }
            }

        }

    }

}
