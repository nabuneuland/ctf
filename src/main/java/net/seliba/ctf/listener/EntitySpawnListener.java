package net.seliba.ctf.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Class which handles Entity spawns.
 */
public class EntitySpawnListener implements Listener {

    /**
     * Executed when an Entity spawns.
     * Used to disable most spawns.
     * @param event The EntitySpawnEvent, provided by Spigot
     */
    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent event) {

        //Check if the spawned Entity is not an Arrow or Snowball
        if(event.getEntityType() != EntityType.ARROW && event.getEntityType() != EntityType.SNOWBALL) {
            //Cancel the event
            event.setCancelled(true);
        }

    }

}
