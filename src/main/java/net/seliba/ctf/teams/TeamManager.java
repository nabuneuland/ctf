package net.seliba.ctf.teams;

import net.seliba.ctf.Ctf;
import net.seliba.ctf.utils.ItemStackBuilder;
import net.seliba.weapons.Weapons;
import net.seliba.weapons.weapons.Arming;
import net.seliba.weapons.weapons.Rifle;
import net.seliba.weapons.weapons.Shotgun;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Class which handles any operations
 * on the teams.
 * @see Team
 */
public class TeamManager {

    private final Set<Player> blueTeamPlayers = new HashSet<>();
    private final Set<Player> redTeamPlayers = new HashSet<>();
    private final Random random = new Random();

    /**
     * Automatically assigns a Player to a team.
     * @param player The player who should be assigned to a team
     */
    public void joinFair(Player player) {

        //Compare both teams
        int difference = Integer.compare(blueTeamPlayers.size(), redTeamPlayers.size());

        //Switch between the difference
        switch (difference) {
            case 0:
                //Join random team, both teams have the same amount of Players
                int randomTeamNumber = random.nextInt(2);
                if(randomTeamNumber == 0) {
                    join(player, Team.BLUE);
                } else {
                    join(player, Team.RED);
                }
                break;
            case -1:
                //Join team blue
                join(player, Team.BLUE);
                break;
            default:
                //Join team red
                join(player, Team.RED);
                break;
        }

    }

    /**
     * Assigns a Player to a certain team.
     * @param player The Player who should join the team
     * @param team The team that the Player should join
     */
    private void join(Player player, Team team) {

        switch (team) {
            case BLUE:
                blueTeamPlayers.add(player);
                break;
            case RED:
                redTeamPlayers.add(player);
                break;
            default:
                break;
        }

    }

    /**
     * Removes a Player from both teams.
     * @param player The Player who should leave
     */
    public void leave(Player player) {

        //Remove the Player from both Lists
        blueTeamPlayers.remove(player);
        redTeamPlayers.remove(player);

    }

    /**
     * Accesses the team of a Player.
     * @param player The Player whom team should be accessed
     * @return The team of the Player, null if he doesn't have one
     */
    public Team getTeam(Player player) {

        if(blueTeamPlayers.contains(player)) {
            return Team.BLUE;
        } else if(redTeamPlayers.contains(player)) {
            return Team.RED;
        } else {
            return null;
        }

    }

    /**
     * Sets the flag to the flag location of the team.
     * @param team The team whom flag should be reset
     */
    public void resetFlag(Team team) {

        team.getFlagLocation().getBlock().setType(team.getBannerMaterial());

    }


    public void resetPlayer(Player player) {

        //Access the team of the Player
        Team team = getTeam(player);

        //Apply default settings to the Player
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setGameMode(GameMode.SURVIVAL);

        //Add ItemStacks to Inventory
        player.getInventory().setHeldItemSlot(0);
        player.getInventory().addItem(new ItemStackBuilder(Material.STONE_SWORD).setName("§aSteinschwert").build());
        player.getInventory().setItemInOffHand(new ItemStackBuilder(Material.SHIELD).setName("§aSchild").build());

        //Optional: Add weapons to Inventory (plugin by stnwtr)
        addWeapons(player);

        //Set Chestplate of the Player
        player.getInventory().setChestplate(new ItemStackBuilder(Material.LEATHER_CHESTPLATE).setColor(team.getDyeColor()).build());

        //Teleport the Player with a short delay (bugfix) to his team spawn location
        Bukkit.getScheduler().runTaskLater(Ctf.getProvidingPlugin(Ctf.class), () ->
            player.teleport(team.getSpawnPointLocation())
        , 3L);

    }

    /**
     * Adds weapons from stnwtr's Weapon plugin to the Players's inventory
     * @param player The Player who should get the Weapons
     */
    private void addWeapons(Player player) {

        //Add Rifle
        Rifle rifle = new Rifle();
        player.getInventory().addItem(rifle.getItemStack());
        Arming.getPlayer(player).setRifle(rifle);
        Weapons.getWeaponList().add(rifle);

        //Add Shotgun
        Shotgun shotgun = new Shotgun();
        player.getInventory().addItem(shotgun.getItemStack());
        Arming.getPlayer(player).setShotgun(shotgun);
        Weapons.getWeaponList().add(shotgun);

    }

}
