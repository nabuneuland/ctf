package net.seliba.ctf.teams;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;

public enum Team {

  BLUE("§1Blau", Color.BLUE, Material.BLUE_BANNER, new Location(Bukkit.getWorld("CTF-New"), -52.5D, 3.8125D, 280.5D, 180f, 0f), new Location(Bukkit.getWorld("CTF-New"), -53.0, 4D, 266D)),
  RED("§cRot", Color.RED, Material.RED_BANNER, new Location(Bukkit.getWorld("CTF-New"), -52.5D, 3.8125D, 154.5D, 0f, 0f), new Location(Bukkit.getWorld("CTF-New"), -53D, 4D, 168D));

  private String name;
  private Color dyeColor;
  private Material bannerMaterial;
  private Location spawnPointLocation;
  private Location flagLocation;

    /**
     * Default constructor
     * @param name The translated and formated name of the team
     * @param dyeColor The color of the team
     * @param bannerMaterial The material of the banner from the team
     * @param spawnPointLocation The location where the team should spawn
     * @param flagLocation The location of the flag from the team
     */
  Team(String name, Color dyeColor, Material bannerMaterial, Location spawnPointLocation, Location flagLocation) {

      this.name = name;
      this.dyeColor = dyeColor;
      this.bannerMaterial = bannerMaterial;
      this.spawnPointLocation = spawnPointLocation;
      this.flagLocation = flagLocation;

  }

    /**
     * Accesses the name of the team
     * @return The name
     */
  public String getName() {
      return name;
  }

    /**
     * Accesses the Color of the team
     * @return The Color
     */
  public Color getDyeColor() {
      return dyeColor;
  }

    /**
     * Accesses he Material of the flag
     * @return The Material
     */
  public Material getBannerMaterial() {
      return bannerMaterial;
  }

    /**
     * Accesses the spawn location of the team
     * @return The spawn location
     */
  public Location getSpawnPointLocation() {
      return spawnPointLocation;
  }

    /**
     * Accesses the flag location of the team
     * @return The flag location
     */
  public Location getFlagLocation() {
      return flagLocation;
  }

}
