package net.seliba.ctf.points;

public enum PointReason {

    KILL(1),
    FLAGMAN_KILL(2),
    FLAG_CAPTURE(10),
    FLAG_PICKUP(1),
    FLAG_RECOVER(1);

    private int points;

    /**
     * The default constructor
     * @param points Amount of points
     */
    PointReason(int points) {

        this.points = points;

    }

    /**
     * Returns the amount of coins which
     * the reason has.
     * @return The amount of coins from the reason
     */
    public int getPoints() {

        return points;

    }

}
